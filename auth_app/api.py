from flask import Blueprint, request, jsonify
from flask_caching import Cache
from marshmallow import ValidationError, RAISE
from .models import db, Group, Resource
from .schemas import ResourceSchema, GroupSchema, UserSchema, PermissionSchema, UserGroup

api = Blueprint('api', __name__)
cache = Cache()


def parse_pagination_query_args(query, default_page=1, default_page_size=20, max_page_size=200):
    page = query.get('page')
    page = int(page) if page and page.isdigit() and int(page) > 0 else default_page
    page_size = query.get('page_size')
    page_size = int(page_size) if page_size and page_size.isdigit() and 0 < int(page_size) < max_page_size \
        else default_page_size
    return page, page_size


def paginate_response(results, schema):
    # formulate response
    response = dict()
    meta = response["meta"] = dict()
    meta["page_count"] = results.pages
    meta["total_count"] = results.total
    meta["page"] = results.page
    meta["per_page"] = results.per_page
    response["records"] = schema(many=True).dump(results.items)
    return response


# Application API

# Resources
@api.route('/resource/<resource_id>', methods=['GET'])
def get_resource(resource_id):
    resource = Resource.query.get(resource_id)
    if resource is None:
        return '', 404
    return jsonify(ResourceSchema().dump(resource))


@api.route('/resource', methods=['POST'])
def create_resource():
    try:
        schema = ResourceSchema()
        resource = schema.load(request.json, unknown=RAISE)
        db.session.add(resource)
        db.session.commit()
        return jsonify(schema.dump(resource))
    except ValidationError as error:
        return jsonify(error.messages), 400
    except Exception as exception:
        db.session.rollback()
        db.session.flush()
        return '', 409


@api.route('/resource', methods=['GET'])
def get_all_resources():
    # validate Query
    page, page_size = parse_pagination_query_args(request.args)
    # query database
    resources = Resource.query.paginate(page, page_size, error_out=False)
    # formulate response
    response = paginate_response(resources, ResourceSchema)
    return jsonify(response), 200


# Groups
@api.route('/group', methods=['POST'])
def create_group():
    try:
        schema = GroupSchema()
        group = schema.load(request.json, unknown=RAISE)
        db.session.add(group)
        db.session.commit()
        return jsonify(schema.dump(group))
    except ValidationError as error:
        return jsonify(error.messages), 400
    except Exception as exception:
        db.session.rollback()
        db.session.flush()
        return '', 409


@api.route('/group/<group_id>', methods=['GET'])
def get_group_by_id(group_id):
    group = Group.query.get(group_id)
    if group is None:
        return '', 404
    return jsonify(GroupSchema().dump(group))


@api.route('/group', methods=['GET'])
def get_all_groups():
    # validate Query
    page, page_size = parse_pagination_query_args(request.args)
    # query database
    groups = Group.query.paginate(page, page_size, error_out=False)
    # formulate response
    response = paginate_response(groups, GroupSchema)
    return jsonify(response), 200


# listing/adding users to groups
@api.route('/group/<group_id>/user', methods=['POST'])
def attach_users_to_group(group_id):
    # check if the group exists
    group = Group.query.get(group_id)
    if group is None:
        return '', 404
    try:
        # validate the user ids
        users = UserSchema(many=True).load(request.json, unknown=RAISE)
        # attach users to group
        group.users.extend(users)
        db.session.add_all(users)
        db.session.commit()
        return '', 204
    except ValidationError as error:
        return jsonify(error.messages), 400
    except Exception as exception:
        db.session.rollback()
        db.session.flush()
        return '', 409


@api.route('/group/<group_id>/user', methods=['GET'])
def get_group_users(group_id):
    # check if the group exists
    group = Group.query.get(group_id)
    if group is None:
        return '', 404
    # validate Query
    page, page_size = parse_pagination_query_args(request.args)
    # paginate on users
    users = group.users.paginate(page, page_size, error_out=False)
    # formulate response
    response = paginate_response(users, UserSchema)
    return jsonify(response), 200


# setting/listing group authorization
@api.route('/group/<group_id>/authorize', methods=['POST'])
def authorize_group(group_id):
    # check if the group exists
    group = Group.query.get(group_id)
    if group is None:
        return '', 404
    try:
        # validate the json input
        resources = list()
        for resource in PermissionSchema(many=True).load(request.json, unknown=RAISE):
            # check if the resources exist
            resource = Resource.query.get(resource["id"])
            if resource:
                resources.append(resource)
            else:
                return '', 404

        # attach resources to group
        group.resources.extend(resources)
        db.session.commit()
        return '', 204
    except ValidationError as error:
        return jsonify(error.messages), 400
    except Exception as exception:
        db.session.rollback()
        db.session.flush()
        return '', 409


@api.route('/group/<group_id>/resource', methods=['GET'])
def get_group_resources(group_id):
    # check if the group exists
    group = Group.query.get(group_id)
    if group is None:
        return '', 404
    # validate Query
    page, page_size = parse_pagination_query_args(request.args)
    # paginate on resources
    resources = group.resources.paginate(page, page_size, error_out=False)
    # formulate response
    response = paginate_response(resources, ResourceSchema)
    return jsonify(response), 200


# Authenticate
@api.route('/authorized', methods=['GET'])
def authorize():
    user_id = request.args.get("userId", None)
    resource_name = request.args.get("resourceName", None)
    # validate on query strings
    if any([item is None or item == '' for item in [user_id, resource_name]]):
        return '', 400

    @cache.memoize(timeout=60)
    def is_authorized(u_id, r_name):
        results = UserGroup.query.join(Group, UserGroup.group_id == Group.id).join(Resource, Group.resources)\
            .filter(UserGroup.user_id == u_id).filter(Resource.name == r_name).all()
        if results:
            return jsonify({"authorized": True}), 200
        else:
            return jsonify({"authorized": False}), 403
    return is_authorized(user_id, resource_name)
