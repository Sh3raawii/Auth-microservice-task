from marshmallow import Schema, fields, post_load, post_dump
from auth_app.models import Resource, Group, UserGroup


class ResourceSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)

    @post_load
    def create_resource(self, data):
        return Resource(name=data.get('name'))


class GroupSchema(Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True)
    description = fields.String(required=False)

    @post_load
    def create_group(self, data):
        return Group(name=data.get('name'), description=data.get('description'))

    @post_dump
    def clear_missing(self, data):
        keys = list(filter(lambda k: data[k] is None, data.keys()))
        for key in keys:
            data.pop(key)
        return data


class UserSchema(Schema):
    userId = fields.String(required=True, attribute="user_id", validate=str.isalnum)

    @post_load
    def create_user_group(self, data):
        return UserGroup(user_id=data.get('user_id'))


class PermissionSchema(Schema):
    resourceId = fields.Integer(required=True, attribute="id")
