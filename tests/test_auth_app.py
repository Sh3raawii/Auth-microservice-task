import unittest
import json
from auth_app import create_app
from auth_app.models import db, Resource, Group, UserGroup
from config import TestingConfig


class ResourceTestCases(unittest.TestCase):
    """This class represents the Resource model test cases"""

    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def test_resource_creation_valid_body(self):
        res = self.client().post('/resource', data=json.dumps({"name": "test"}), content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertIn("id", res.json)
        self.assertEqual(1, res.json.get("id"))
        self.assertIn("name", res.json)
        self.assertEqual("test", res.json.get("name"))
        with self.app.app_context():
            record = Resource.query.get(res.json.get("id"))
            self.assertEqual(record.name, "test")

    def test_resource_creation_invalid_body(self):
        res = self.client().post('/resource', data=json.dumps({"name": "test", "invalid_key": "value"}),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_resource_creation_missing_data(self):
        res = self.client().post('/resource', data=json.dumps({}), content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_get_existing_resource_by_id(self):
        with self.app.app_context():
            db.session.add(Resource(name='bus1'))
            db.session.commit()
        res = self.client().get('/resource/1')
        self.assertEqual(res.status_code, 200)
        self.assertIn("id", res.json)
        self.assertEqual(1, res.json.get("id"))
        self.assertIn("name", res.json)
        self.assertEqual("bus1", res.json.get("name"))

    def test_get_non_existent_resource_by_id(self):
        res = self.client().get('/resource/100')
        self.assertEqual(res.status_code, 404)

    def test_get_all_resources(self):
        with self.app.app_context():
            db.session.add(Resource(name='bus1'))
            db.session.add(Resource(name='bus2'))
            db.session.add(Resource(name='line1'))
            db.session.add(Resource(name='booking1'))
            db.session.commit()
        res = self.client().get('/resource?page=2&page_size=2')
        self.assertIn("meta", res.json)
        self.assertIn("records", res.json)
        self.assertIsInstance(res.json.get("records"), list)
        self.assertIn("page", res.json.get("meta"))
        self.assertIn("per_page", res.json.get("meta"))
        self.assertIn("page_count", res.json.get("meta"))
        self.assertIn("total_count", res.json.get("meta"))
        self.assertEqual(res.json["meta"]["page"], 2)
        self.assertEqual(res.json["meta"]["per_page"], 2)
        self.assertEqual(res.json["meta"]["page_count"], 2)
        self.assertEqual(res.json["meta"]["total_count"], 4)
        for resource in res.json["records"]:
            self.assertIsInstance(resource, dict)
            self.assertIn("name", resource)
            self.assertIn("id", resource)

    def test_get_all_resources_empty(self):
        # testing response structure when empty
        res = self.client().get('/resource')
        self.assertIn("meta", res.json)
        self.assertIn("records", res.json)
        self.assertIn("page", res.json.get("meta"))
        self.assertIn("per_page", res.json.get("meta"))
        self.assertIn("page_count", res.json.get("meta"))
        self.assertIn("total_count", res.json.get("meta"))
        self.assertEqual(res.json["meta"]["page"], 1)
        self.assertEqual(res.json["meta"]["per_page"], 20)
        self.assertEqual(res.json["meta"]["page_count"], 0)
        self.assertEqual(res.json["meta"]["total_count"], 0)
        self.assertEqual(res.json.get("records"), [])

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


class GroupTestCases(unittest.TestCase):
    """This class represents the Group model test cases"""

    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def test_group_creation_without_optional_fields(self):
        res = self.client().post('/group', data=json.dumps({"name": "group1"}), content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertIn("id", res.json)
        self.assertIn("name", res.json)
        self.assertNotIn("description", res.json)
        self.assertEqual("group1", res.json.get("name"))
        with self.app.app_context():
            record = Group.query.get(res.json.get("id"))
            self.assertEqual(record.name, "group1")
            self.assertEqual(record.description, None)

    def test_group_creation_with_optional_fields(self):
        res = self.client().post('/group', data=json.dumps({"name": "group2", "description": "dummy description"}),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertIn("id", res.json)
        self.assertIn("name", res.json)
        self.assertIn("description", res.json)
        self.assertEqual("group2", res.json.get("name"))
        self.assertEqual("dummy description", res.json.get("description"))
        with self.app.app_context():
            record = Group.query.get(res.json.get("id"))
            self.assertEqual(record.name, "group2")
            self.assertEqual(record.description, "dummy description")

    def test_group_creation_with_invalid_fields(self):
        res = self.client().post('/group', data=json.dumps({"name": "test", "invalid_key": "dummy_value"}),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_group_creation_with_missing_fields(self):
        res = self.client().post('/group', data=json.dumps({"description": "dummy description"}),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_get_existing_group_by_id(self):
        with self.app.app_context():
            db.session.add(Group(name='group1', description="dummy description"))
            db.session.commit()
        res = self.client().get('/group/1')
        self.assertEqual(res.status_code, 200)
        self.assertIn("id", res.json)
        self.assertEqual(1, res.json.get("id"))
        self.assertIn("name", res.json)
        self.assertEqual("group1", res.json.get("name"))
        self.assertIn("description", res.json)
        self.assertEqual("dummy description", res.json.get("description"))

    def test_get_non_existent_group_by_id(self):
        res = self.client().get('/group/100')
        self.assertEqual(res.status_code, 404)

    def test_get_all_groups(self):
        with self.app.app_context():
            db.session.add(Group(name='group1', description="dummy description 1"))
            db.session.add(Group(name='group2', description="dummy description 2"))
            db.session.add(Group(name='group3', description="dummy description 3"))
            db.session.commit()
        res = self.client().get('/group?page=2&page_size=2')
        self.assertIn("meta", res.json)
        self.assertIn("records", res.json)
        self.assertIsInstance(res.json.get("records"), list)
        self.assertIn("page", res.json.get("meta"))
        self.assertIn("per_page", res.json.get("meta"))
        self.assertIn("page_count", res.json.get("meta"))
        self.assertIn("total_count", res.json.get("meta"))
        self.assertEqual(res.json["meta"]["page"], 2)
        self.assertEqual(res.json["meta"]["per_page"], 2)
        self.assertEqual(res.json["meta"]["page_count"], 2)
        self.assertEqual(res.json["meta"]["total_count"], 3)
        self.assertEqual(len(res.json["records"]), 1)
        for group in res.json["records"]:
            self.assertIsInstance(group, dict)
            self.assertIn("description", group)
            self.assertIn("name", group)
            self.assertIn("id", group)

    def test_get_all_groups_empty(self):
        # testing response structure when empty
        res = self.client().get('/group')
        self.assertIn("meta", res.json)
        self.assertIn("records", res.json)
        self.assertIn("page", res.json.get("meta"))
        self.assertIn("per_page", res.json.get("meta"))
        self.assertIn("page_count", res.json.get("meta"))
        self.assertIn("total_count", res.json.get("meta"))
        self.assertEqual(res.json["meta"]["page"], 1)
        self.assertEqual(res.json["meta"]["per_page"], 20)
        self.assertEqual(res.json["meta"]["page_count"], 0)
        self.assertEqual(res.json["meta"]["total_count"], 0)
        self.assertEqual(res.json.get("records"), [])

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


class UserGroupTestCases(unittest.TestCase):

    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def test_adding_users_to_existent_group(self):
        with self.app.app_context():
            db.session.add(Group(name='group1', description="dummy description"))
            db.session.commit()
        res = self.client().post('/group/1/user',
                                 data=json.dumps([{"userId": "qwerty"}, {"userId": "qwerty123"}, {"userId": "12345"}]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 204)
        with self.app.app_context():
            group = Group.query.get(1)
            self.assertCountEqual([user.user_id for user in group.users], ["qwerty", "qwerty123", "12345"])

    def test_adding_users_to_non_existent_group(self):
        res = self.client().post('/group/100/user',
                                 data=json.dumps([{"userId": "dummyId"}]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 404)

    def test_adding_existing_users_to_group(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            user = UserGroup(user_id="user1")
            group.users.append(user)
            db.session.add(group)
            db.session.add(user)
            db.session.commit()
        res = self.client().post('/group/1/user',
                                 data=json.dumps([{"userId": "user1"}, {"userId": "user2"}]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 409)
        with self.app.app_context():
            group = Group.query.get(1)
            self.assertCountEqual([user.user_id for user in group.users], ["user1"])

    def test_adding_users_to_group_with_invalid_fields(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            db.session.add(group)
            db.session.commit()
        res = self.client().post('/group/1/user',
                                 data=json.dumps([{"userId": "user1", "name": "ahmed"}, {"userId": "user2"}]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)
        with self.app.app_context():
            group = Group.query.get(1)
            self.assertCountEqual([user.user_id for user in group.users], [])

    def test_get_users_in_group(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description 1")
            user1 = UserGroup(user_id="user1")
            user2 = UserGroup(user_id="user2")
            user3 = UserGroup(user_id="user3")
            group.users.extend([user1, user2, user3])
            db.session.add(group)
            db.session.add_all([user1, user2, user3])
            db.session.commit()
        res = self.client().get('/group/1/user?page=2&page_size=2')
        self.assertIn("meta", res.json)
        self.assertIn("records", res.json)
        self.assertIsInstance(res.json.get("records"), list)
        self.assertIn("page", res.json.get("meta"))
        self.assertIn("per_page", res.json.get("meta"))
        self.assertIn("page_count", res.json.get("meta"))
        self.assertIn("total_count", res.json.get("meta"))
        self.assertEqual(res.json["meta"]["page"], 2)
        self.assertEqual(res.json["meta"]["per_page"], 2)
        self.assertEqual(res.json["meta"]["page_count"], 2)
        self.assertEqual(res.json["meta"]["total_count"], 3)
        self.assertEqual(len(res.json["records"]), 1)
        for group in res.json["records"]:
            self.assertIsInstance(group, dict)
            self.assertIn("userId", group)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


class GroupPermissionTestCases(unittest.TestCase):

    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def test_giving_permission_to_existent_resources(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            db.session.add(group)
            db.session.add_all([bus1, bus2])
            db.session.commit()
            group_id = group.id
            resource_ids = [bus1.id, bus2.id]
        res = self.client().post('/group/{}/authorize'.format(group_id),
                                 data=json.dumps([{"resourceId": resource_id} for resource_id in resource_ids]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 204)
        with self.app.app_context():
            group = Group.query.get(group_id)
            self.assertCountEqual([resource.id for resource in group.resources], resource_ids)

    def test_giving_permission_to_non_existent_resources(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            db.session.add(group)
            db.session.commit()
            group_id = group.id
        res = self.client().post('/group/{}/authorize'.format(group_id),
                                 data=json.dumps([{"resourceId": 100}]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 404)
        with self.app.app_context():
            group = Group.query.get(group_id)
            self.assertCountEqual([resource.id for resource in group.resources], [])

    def test_giving_permission_to_non_existent_group(self):
        with self.app.app_context():
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            db.session.add_all([bus1, bus2])
            db.session.commit()
            resource_ids = [bus1.id, bus2.id]
        res = self.client().post('/group/1/authorize',
                                 data=json.dumps([{"resourceId": resource_id} for resource_id in resource_ids]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 404)
        with self.app.app_context():
            for resource_id in resource_ids:
                resource = Resource.query.get(resource_id)
                self.assertCountEqual(resource.groups, [])

    def test_giving_permission_invalid_json(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            db.session.add(group)
            db.session.add_all([bus1, bus2])
            db.session.commit()
            group_id = group.id
            resource_ids = [bus1.id, bus2.id]
        res = self.client().post('/group/{}/authorize'.format(group_id),
                                 data=json.dumps([{"resourceId": resource_id, "name": "line"}
                                                  for resource_id in resource_ids]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 400)
        with self.app.app_context():
            group = Group.query.get(group_id)
            self.assertCountEqual(group.resources, [])

    def test_duplicate_permissions(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group.resources.append(bus1)
            db.session.add(group)
            db.session.add(bus2)
            db.session.commit()
            group_id = group.id
            resource_ids = [bus1.id, bus2.id]
        res = self.client().post('/group/{}/authorize'.format(group_id),
                                 data=json.dumps([{"resourceId": resource_id} for resource_id in resource_ids]),
                                 content_type='application/json')
        self.assertEqual(res.status_code, 409)
        with self.app.app_context():
            group = Group.query.get(group_id)
            self.assertCountEqual([resource.id for resource in group.resources], [bus1.id])

    def test_get_group_permissions(self):
        with self.app.app_context():
            group = Group(name='group1', description="dummy description")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group.resources.extend([bus1, bus2])
            db.session.add(group)
            db.session.commit()
            group_id = group.id
            resource_ids = [bus1.id, bus2.id]
        res = self.client().get('/group/{}/resource?page=1&page_size=2'.format(group_id))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['meta']['total_count'], 2)
        for resource in res.json.get("records"):
            self.assertIn("id", resource)
            self.assertIn("name", resource)
            self.assertIn(resource['id'], resource_ids)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


class AuthenticationTestCases(unittest.TestCase):

    def setUp(self):
        self.app = create_app(TestingConfig)
        self.client = self.app.test_client
        with self.app.app_context():
            db.create_all()

    def test_one_group_with_access(self):
        with self.app.app_context():
            group1 = Group(name='group1', description="dummy description 1")
            group2 = Group(name='group2', description="dummy description 2")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group1.users.append(UserGroup(user_id="user1"))
            group2.users.append(UserGroup(user_id="user1"))
            group1.resources.append(bus2)
            group2.resources.extend([bus1, bus2])
            db.session.add_all([group1, group2, bus1, bus2])
            db.session.commit()
        res = self.client().get('/authorized?userId={}&resourceName={}'.format("user1", "bus1"))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json, {"authorized": True})

    def test_multiple_groups_with_access(self):
        with self.app.app_context():
            group1 = Group(name='group1', description="dummy description 1")
            group2 = Group(name='group2', description="dummy description 2")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group1.users.append(UserGroup(user_id="user1"))
            group2.users.append(UserGroup(user_id="user1"))
            group1.resources.append(bus2)
            group2.resources.extend([bus1, bus2])
            db.session.add_all([group1, group2, bus1, bus2])
            db.session.commit()
        res = self.client().get('/authorized?userId={}&resourceName={}'.format("user1", "bus2"))
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json, {"authorized": True})

    def test_no_group_with_access(self):
        with self.app.app_context():
            group1 = Group(name='group1', description="dummy description 1")
            group2 = Group(name='group2', description="dummy description 2")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group1.users.append(UserGroup(user_id="user1"))
            group1.resources.append(bus2)
            group2.resources.extend([bus1, bus2])
            db.session.add_all([group1, group2, bus1, bus2])
            db.session.commit()
        res = self.client().get('/authorized?userId={}&resourceName={}'.format("user1", "bus1"))
        self.assertEqual(res.status_code, 403)
        self.assertEqual(res.json, {"authorized": False})

    def test_non_existent_user(self):
        with self.app.app_context():
            group1 = Group(name='group1', description="dummy description 1")
            group2 = Group(name='group2', description="dummy description 2")
            bus1 = Resource(name='bus1')
            bus2 = Resource(name='bus2')
            group1.users.append(UserGroup(user_id="user1"))
            group1.resources.append(bus2)
            group2.resources.extend([bus1, bus2])
            db.session.add_all([group1, group2, bus1, bus2])
            db.session.commit()
        res = self.client().get('/authorized?userId={}&resourceName={}'.format("user2", "bus2"))
        self.assertEqual(res.status_code, 403)
        self.assertEqual(res.json, {"authorized": False})

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

if __name__ == '__main__':
    unittest.main()
