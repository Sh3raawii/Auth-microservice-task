# Authorization Micro-service app

- The app is implemented in Python/Flask
- I chose postgres as my datastore and memcached for caching however it should work with any other DBMS or caching backend if configured correctly
- All implemented endpoints handle errors 
- I created an index on the user_id column in the UserGroup Table/Model and another index on the resource name column in the Resource Table/Model for faster lookups
- I created an index on the resource_id column in the permissions table.
- The Authorization endpoint responds in less than 20ms on average for non-cached requests
- Since all implemented endpoints so far don't revoke permissions for groups or remove users from groups, the cache validation is time based and no endpoint removes records from the cache
- All endpoints are tested
- Pagination is implemented in listing endpoints


The authorization response time was tested with modest number of rows, about 300K unique users
- Group Table ~ 100K rows
- UserGroup Table ~ 1.5M rows
- Resource Table ~ 400K
- Permissions Table ~ 3M


### Requirements

* [Python 3] - python 3.6+ is preferred
* [PostgreSQL] 
* [Memcached]

### Installation 

Clone the repository
```sh
~$ git clone git@gitlab.com:Sh3raawii/Auth-microservice-task.git
```
cd into the project directory

```sh
~$ cd Auth-microservice-task
```

Create virtual environment for the project and activate it
```sh
Auth-microservice-task$ python3 -m venv app-env
Auth-microservice-task$ source app-env/bin/activate
(app-env) Auth-microservice-task$ 
```

Now we are ready to install project requirements
```sh
(app-env) Auth-microservice-task$ pip install -r requirements/development.txt
```

Now create your own config file or update the values of existing base configuration, the most important configurations for this app are SQLALCHEMY_DATABASE_URI and CACHE_TYPE

Apply database migrations
```sh
(app-env) Auth-microservice-task$ flask db upgrade
```

(Optional) Set app config - default is base

```sh
(app-env) Auth-microservice-task$ export FLASK_CONFIG_FILE=mostafa
```

Start the app

```sh
(app-env) Auth-microservice-task$ export FLASK_APP=auth_app
(app-env) Auth-microservice-task$ flask run -h "localhost" -p 5000
```


   [Python 3]: <https://www.python.org/download/releases/3.0/>
   [PostgreSQL]: <https://www.postgresql.org/download/>
   [Memcached]: <https://memcached.org/downloads>