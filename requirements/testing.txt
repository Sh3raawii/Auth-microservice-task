Flask==1.0.2
Flask-Caching==1.4.0
Flask-Migrate==2.2.1
Flask-SQLAlchemy==2.3.2
marshmallow==3.0.0b13
nose==1.3.7
psycopg2==2.7.5
python-memcached==1.59