import os


class BaseConfig:
    ENV = "development"
    DEBUG = True
    BASE_DIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
    SECRET_KEY = "development_key"

    # Database Configurations
    SQLALCHEMY_DATABASE_URI = "postgresql://swvl:password@localhost/swvl_auth"
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # Caching
    CACHE_TYPE = "memcached"
